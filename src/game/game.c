#include "game.h"
#include <stdio.h>
#include "../components/player/player.h"
#include "../utility/utility.h"

hge_entity* digits[4];
void createGame()
{
    hge_entity* game = hgeCreateEntity();
    game_component game_data;
    game_data.timer = 0;
    game_data.state = NONE;
    game_data.dif = 0;
    hgeAddComponent(game, hgeCreateComponent("Game", &game_data, sizeof(game_data)));

    spritesheet_component sprite;
    sprite.flipped = false;
    sprite.FPS = 0;
    sprite.frame.x = 9;
    sprite.frame.y = 0;
    sprite.num_frames = 0;
    sprite.playing = false;
    sprite.resolution.x = 6;
    sprite.resolution.y = 7;
    sprite.spritesheet = hgeResourcesQueryTexture("Timer");
    sprite.time = 0;
    hge_vec3 scale = {6, 7, 0};
    hge_vec3 position = {0, 0, 0};
    hge_transform transform = {position, scale};
    digit_component digit;
    digit.value = 0;
    for(int i = 0; i < 4; i++)
    {
        digits[i] = hgeCreateEntity();
        hgeAddComponent(digits[i], hgeCreateComponent("Transform", &transform, sizeof(transform)));
        hgeAddComponent(digits[i], hgeCreateComponent("SpriteSheet", &sprite, sizeof(sprite)));
        hgeAddComponent(digits[i], hgeCreateComponent("Digit", &digit, sizeof(digit)));
    }

}

void GameSystem(hge_entity* e, game_component* data)
{
    if(data->state == NONE)
        playIntro(data);
    if(data->state == ALIVE) {
        data->timer += hgeDeltaTime();
        if(data->timer >= data->dif)
        {
            killPlayer();
            data->state = DEAD;
        }
    }

    calculateTimerDigit(data);
    printTimer();
    hge_entity* cam = hgeQueryEntity(1, "ActiveCamera");
    hge_vec3* cam_position = cam->components[hgeQuery(cam, "Position")].data;
    hge_vec3 position = *cam_position;
    position.z = FAR-1;
    hge_vec3 scale;
    if(data->state == WIN)
    {
        scale.x = 128;
        scale.y = 25;
        scale.z = 0;
        hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), hgeResourcesQueryTexture("Won"), position, scale, 0);
    }
    if(data->state == DEAD)
    {
        scale.x = 128;
        scale.y = 25;
        scale.z = 0;
        hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), hgeResourcesQueryTexture("Died"), position, scale, 0);
    }
}

void calculateTimerDigit(game_component* data)
{
    float timeLeft = data->dif - data->timer;
    int minute;
    int sec;
    int secDigits[2];
    minute = (int)timeLeft / 60;
    sec = (int)timeLeft - minute*60;
    secDigits[1] = sec / 10;
    secDigits[0] = sec - secDigits[1]*10;
    digit_component* digit_data = digits[0]->components[hgeQuery(digits[0], "Digit")].data;
    digit_data->value = minute;
    digit_data = digits[1]->components[hgeQuery(digits[1], "Digit")].data;
    digit_data->value = 0;
    digit_data = digits[2]->components[hgeQuery(digits[2], "Digit")].data;
    digit_data->value = secDigits[1];
    digit_data = digits[3]->components[hgeQuery(digits[3], "Digit")].data;
    digit_data->value = secDigits[0];
}

void printTimer()
{
    for(int i = 0; i < 4; i++)
    {
        digit_component* digit_data = digits[i]->components[hgeQuery(digits[i], "Digit")].data;
        spritesheet_component* sprite_data = digits[i]->components[hgeQuery(digits[i], "SpriteSheet")].data;
        switch(digit_data->value)
        {
            case 1:
                sprite_data->frame.x = 0;
                break;
            case 2:
                sprite_data->frame.x = 1;
                break;
            case 3:
                sprite_data->frame.x = 2;
                break;
            case 4:
                sprite_data->frame.x = 3;
                break;
            case 5:
                sprite_data->frame.x = 4;
                break;
            case 6:
                sprite_data->frame.x = 5;
                break;
            case 7:
                sprite_data->frame.x = 6;
                break;
            case 8:
                sprite_data->frame.x = 7;
                break;
            case 9:
                sprite_data->frame.x = 8;
                break;
            case 0:
                sprite_data->frame.x = 9;
                break;
        }
    }

    spritesheet_component* sprite_data = digits[1]->components[hgeQuery(digits[1], "SpriteSheet")].data;
    sprite_data->frame.x = 10;

    hge_entity* cam = hgeQueryEntity(1, "ActiveCamera");
    hge_vec3* cam_position = cam->components[hgeQuery(cam, "Position")].data;
    hge_vec3 position = *cam_position;
    position.x += (hgeWindowWidth()/2)/5;
    position.y += (hgeWindowHeight()/2)/5 - 6;
    position.z = FAR-1;

    for(int i = 3; i >= 0; i--)
    {
        hge_transform* transform_data = digits[i]->components[hgeQuery(digits[i], "Transform")].data;
        position.x -= 6;
        transform_data->position = position;
    }

    // USE GUI SHADER INSTEAD OF SPRITE !
}

void playIntro(game_component* data)
{
    data->state = INTRO;
    hge_entity* intro = hgeCreateEntity();
    intro_component intro_data;
    intro_data.state = TITLE;
    hgeAddComponent(intro, hgeCreateComponent("Intro", &intro_data, sizeof(intro_data)));
}

void chooseDifficulty(intro_component* data)
{
    bool ONE = hgeInputGetKeyDown(HGE_KEY_1);
    bool TWO = hgeInputGetKeyDown(HGE_KEY_2);
    bool THREE = hgeInputGetKeyDown(HGE_KEY_3);
    bool FOUR = hgeInputGetKeyDown(HGE_KEY_4);

    hge_entity* game = hgeQueryEntity(1, "Game");
    game_component* game_data = game->components[hgeQuery(game, "Game")].data;

    if(ONE){
        data->state = START;
        game_data->dif = EASY;
    } else if(TWO){
        data->state = START;
        game_data->dif = MEDIUM;
    } else if(THREE){
        data->state = START;
        game_data->dif = HARD;
    } else if(FOUR){
        data->state = START;
        game_data->dif = IMPOSSIBLE;
    }
}

void startGame(hge_entity* e)
{
    if(hgeInputGetKeyDown(HGE_KEY_ENTER))
    {
        hgeDestroyEntity(e);
        GameStart();
    }
}

bool isFollowing = false;

void IntroSystem(hge_entity* e, intro_component* data)
{
    if(hgeInputGetKeyDown(HGE_KEY_SPACE) && data->state < DIFFICULTY)
        data->state++;

    hge_entity* cam = hgeQueryEntity(1, "ActiveCamera");
    hge_vec3* cam_position = cam->components[hgeQuery(cam, "Position")].data;
    hge_vec3 position = *cam_position;
    position.z = FAR-1;
    hge_vec3 scale;
    switch(data->state)
    {
        case TITLE:
            scale.x = 256;
            scale.y = 25;
            scale.z = 0;
            hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), hgeResourcesQueryTexture("Title"), position, scale, 0);
            break;
        case TUTORIAL:
            scale.x = 240;
            scale.y = 75;
            scale.z = 0;
            hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), hgeResourcesQueryTexture("Tutorial"), position, scale, 0);
            break;
        case WINNING:
            scale.x = 272;
            scale.y = 100;
            scale.z = 0;
            hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), hgeResourcesQueryTexture("Winning"), position, scale, 0);
            if(!isFollowing)
            {
                follow_component follow;
                follow.lock_x = false;
                follow.lock_y = false;
                follow.lock_z = true;
                follow.speed = 10.f;

                hge_entity* sign = hgeQueryEntity(1, "Sign");

                hge_transform* follow_transform = sign->components[hgeQuery(sign, "Transform")].data;
                hge_vec3* sign_position = &follow_transform->position;
                follow.target_pos = sign_position;

                hge_entity* cam = hgeQueryEntity(1, "ActiveCamera");
                hgeAddComponent(cam, hgeCreateComponent("Follow", &follow, sizeof(follow)));
                isFollowing = true;
            }
            break;
        case DIFFICULTY:
            scale.x = 240;
            scale.y = 108;
            scale.z = 0;
            hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), hgeResourcesQueryTexture("Difficulty"), position, scale, 0);
            chooseDifficulty(data);
            break;
        case START:
            scale.x = 176;
            scale.y = 50;
            scale.z = 0;
            hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), hgeResourcesQueryTexture("Start"), position, scale, 0);
            startGame(e);
            break;
    }
}
