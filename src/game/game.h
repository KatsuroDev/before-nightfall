#ifndef BNF_GAME_H
#define BNF_GAME_H

#include <HGE/HGE_Core.h>

typedef enum {
    EASY = 150, // 2min30sec (150)
    MEDIUM = 135, // 2:15 (135)
    HARD = 105, // 1:45 (105)
    IMPOSSIBLE = 75 // 1:15 (75)
} game_difficulty;

typedef enum {
    NONE,
    INTRO,
    ALIVE,
    DEAD,
    WIN
} game_state;

typedef struct {
    float timer;
    game_state state;
    game_difficulty dif;
} game_component;

typedef enum {
    TITLE,
    TUTORIAL,
    WINNING,
    DIFFICULTY,
    START
} intro_state;


typedef struct {
    intro_state state;
} intro_component;

typedef struct {
    int value;
} digit_component;

void createGame();
void GameSystem(hge_entity* e, game_component* data);
void calculateTimerDigit(game_component* data);
void printTimer();
void playIntro(game_component* data);
void IntroSystem(hge_entity* e, intro_component* data);

#endif
