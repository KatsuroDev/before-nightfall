#ifndef BNF_PLAYER_H
#define BNF_PLAYER_H

#include <HGE/HGE_Core.h>
#include <chipmunk/chipmunk.h>
#include <stdbool.h>
#include "../character/character.h"

typedef struct {
    hge_vec2 sprite_res;
    hge_vec2 sprite_frame;
} player_component;

void createPlayer(float x, float y, bool isPlayable);

void PlayableSystem(hge_entity* e, tag_component* tag, character_component* character_data);

void PlayerSystem(hge_entity* e, hge_transform* transform, player_component* player_data);

void killPlayer();

#endif
