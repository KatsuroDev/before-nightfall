#include "player.h"
#include "../../utility/utility.h"
#include "../../physics/physics.h"
#include "../tree/tree.h"
#include "../wood_log/wood_log.h"
#include <stdio.h>

void createPlayer(float x, float y, bool isPlayable)
{
    hge_entity* e = hgeCreateEntity();

    // TRANSFORM
    hge_transform transform;
    transform.position.x = x;
    transform.position.y = y;
    transform.position.z = 0;
    transform.scale.x = TILE;
    transform.scale.y = TILE;
    transform.scale.z = 0;
    hgeAddComponent(e, hgeCreateComponent("Transform", &transform, sizeof(transform)));

    // CHARACTER
    character_component character_data;
    character_data.state = IDLE;
    character_data.movement_direction.x = 0;
    character_data.movement_direction.y = 0;
    character_data.movement_vector.x = 0;
    character_data.movement_vector.y = 0;
    character_data.walk_speed = 50.f;
    character_data.sprint_speed = 100.f;
    character_data.isRunning = false;


        // PHYSICS
        hge_entity* world = hgeQueryEntity(1, "Physic World");
        chipmunk_space* cmS = world->components[hgeQuery(world, "Chipmunk Space")].data;

        cpFloat mass = 1;
        cpFloat moment = cpMomentForBox(mass, transform.scale.x/2, transform.scale.y/2);

        character_data.cmB.body = cpSpaceAddBody(cmS->space, cpBodyNew(mass, moment));
        cpBodySetPosition(character_data.cmB.body, cpv(transform.position.x, transform.position.y));
        addBodyToWorld(character_data.cmB.body);

        character_data.cmSp.shape = cpSpaceAddShape(cmS->space, cpBoxShapeNew(character_data.cmB.body, transform.scale.x/2, transform.scale.y/2, 0));
        cpShapeSetFriction(character_data.cmSp.shape, 0.7);
        addShapeToWorld(character_data.cmSp.shape);

    hgeAddComponent(e, hgeCreateComponent("Character", &character_data, sizeof(character_data)));


    // FOLLOW
    /*follow_component follow;
    follow.lock_x = false;
    follow.lock_y = false;
    follow.lock_z = true;
    follow.speed = 10.f;*/

    hge_transform* follow_transform = e->components[hgeQuery(e, "Transform")].data;
    hge_vec3* player_position = &follow_transform->position;

    hge_entity* cam = hgeQueryEntity(1, "ActiveCamera");
    follow_component* follow_data = cam->components[hgeQuery(cam, "Follow")].data;

    follow_data->target_pos = player_position;
    //hgeAddComponent(cam, hgeCreateComponent("Follow", &follow, sizeof(follow)));

    // PLAYER
    player_component player_data;
    player_data.sprite_res.x = 32;
    player_data.sprite_res.y = 32;
    player_data.sprite_frame.x = 0;
    player_data.sprite_frame.y = 0;

    hgeAddComponent(e, hgeCreateComponent("Player", &player_data, sizeof(player_data)));
    tag_component playable_tag;
    if(isPlayable)
    hgeAddComponent(e, hgeCreateComponent("Playable", &playable_tag, sizeof(playable_tag)));
    spritesheet_component sprite;
    sprite.flipped = false;
    sprite.FPS = 0;
    sprite.frame = player_data.sprite_frame;
    sprite.num_frames = 0;
    sprite.playing = true;
    sprite.resolution = player_data.sprite_res;
    sprite.spritesheet = hgeResourcesQueryTexture("characters");
    sprite.time = 0;
    hgeAddComponent(e, hgeCreateComponent("SpriteSheet", &sprite, sizeof(sprite)));

    // Add Birthday hat to lukas' player.

}


void PlayableSystem(hge_entity* e, tag_component* tag, character_component* character_data)
{

    hge_vec2 direction_vector = {0, 0};

    bool RIGHT = hgeInputGetKey(HGE_KEY_D);
    bool LEFT = hgeInputGetKey(HGE_KEY_A);
    bool UP = hgeInputGetKey(HGE_KEY_W);
    bool DOWN = hgeInputGetKey(HGE_KEY_S);


    if(LEFT || RIGHT || UP || DOWN)
        character_data->state = MOVING;
    if(!(LEFT || RIGHT || UP || DOWN))
        character_data->state = IDLE;

    if(hgeInputGetKey(HGE_KEY_LEFT_SHIFT))
        character_data->isRunning = true;
    else
        character_data->isRunning = false;


    switch(character_data->state)
    {
        case IDLE:
            direction_vector.x = 0;
            direction_vector.y = 0;
            break;
        case MOVING:
            if(LEFT)
                direction_vector.x = -1;
            if(RIGHT)
                direction_vector.x = 1;
            if(UP)
                direction_vector.y = 1;
            if(DOWN)
                direction_vector.y = -1;

            direction_vector = hgeMathVec2Normalize(direction_vector);
            character_data->movement_direction = direction_vector;
            break;
    }
}

void interact_function(hge_entity* e)
{
    bool interacting = hgeInputGetKeyDown(HGE_KEY_SPACE);
    tree_component* chopped_data = e->components[hgeQuery(e, "Tree")].data;
    if(chopped_data->tree && interacting) {
        cutTree(e, chopped_data);
    }

}

void PlayerSystem(hge_entity* e, hge_transform* transform, player_component* player_data)
{
    /*if(hgeInputGetKeyDown(HGE_KEY_E))
    {
        createWoodLog(transform->position.x + TILE, transform->position.y);
    }*/
    //transform->position.z = (transform->position.y + transform->scale.y/2)/TILE;
    transform->position.z = (fabs(transform->position.y - transform->scale.y/2.f)/1000.f) * 200.f - 100.f;

    hge_transform player_interactable_area = *transform;
    player_interactable_area.scale.x = transform->scale.x*2;
    player_interactable_area.scale.y = transform->scale.y*2;


    hge_ecs_request request = hgeECSRequest(1, "Tree");
    for(int i = 0; i < request.NUM_ENTITIES; i++) {
        hge_transform* chopped_transform = request.entities[i]->components[hgeQuery(request.entities[i], "Transform")].data;
        if(AABB(player_interactable_area, *chopped_transform)) {
            interact_function(request.entities[i]);
        }
    }
}

void killPlayer()
{
    hge_entity* player = hgeQueryEntity(1, "Player");
    removeComponent(player, hgeQuery(player, "Playable"));
    character_component* player_data = player->components[hgeQuery(player, "Character")].data;
    player_data->state = IDLE;
}
