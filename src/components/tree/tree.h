#ifndef BNF_TREE_H
#define BNF_TREE_H

#include <HGE/HGE_Core.h>
#include "../../physics/physics.h"

typedef struct {
    chipmunk_body cmB;
    chipmunk_shape cmSp;
    hge_entity* tree;
}   tree_component;

void createTree(float x, float y);

void cutTree(hge_entity* target, tree_component* data);

//void ChoppedSystem(hge_entity* e, hge_transform* transform, tag_component* chopped);

#endif
