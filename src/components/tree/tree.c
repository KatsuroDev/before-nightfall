#include "tree.h"
#include "../../utility/utility.h"
#include "../wood_log/wood_log.h"


void createTree(float x, float y)
{
    hge_entity* chopped = hgeCreateEntity();
    hge_transform c_transform;
    c_transform.position.x = x;
    c_transform.position.y = y;
    c_transform.position.z = NEAR+1;
    c_transform.scale.x = TILE*3;
    c_transform.scale.y = TILE;
    c_transform.scale.z = 0;

    spritesheet_component c_sprite;
    c_sprite.flipped = false;
    c_sprite.FPS = 14;
    c_sprite.frame.x = 0;
    c_sprite.frame.y = 0;
    c_sprite.num_frames = 13;
    c_sprite.playing = true;
    c_sprite.resolution.x = 32*3;
    c_sprite.resolution.y = 32;
    c_sprite.spritesheet = hgeResourcesQueryTexture("Chopped");
    c_sprite.time = 0;

    tree_component tree_data;

    // PHYSICS
        hge_entity* world = hgeQueryEntity(1, "Physic World");
        chipmunk_space* cmS = world->components[hgeQuery(world, "Chipmunk Space")].data;

        tree_data.cmB.body = cpSpaceAddBody(cmS->space, cpBodyNewStatic());
        cpBodySetPosition(tree_data.cmB.body, cpv(c_transform.position.x, c_transform.position.y+c_transform.scale.y/2));
        addBodyToWorld(tree_data.cmB.body);

        tree_data.cmSp.shape = cpSpaceAddShape(cmS->space, cpBoxShapeNew(tree_data.cmB.body, c_transform.scale.x/3, c_transform.scale.y/2, 0));
        cpShapeSetFriction(tree_data.cmSp.shape, 1);
        addShapeToWorld(tree_data.cmSp.shape);

    hgeAddComponent(chopped, hgeCreateComponent("Transform", &c_transform, sizeof(c_transform)));
    hgeAddComponent(chopped, hgeCreateComponent("SpriteSheet", &c_sprite, sizeof(c_sprite)));



    hge_entity* tree = hgeCreateEntity();
    hge_transform transform;
    transform.scale.x = TILE*3;
    transform.scale.y = TILE*3;
    transform.scale.z = 0;
    transform.position.x = x;
    transform.position.y = y + TILE;
    transform.position.z = (fabs(transform.position.y - transform.scale.y/3.f)/1000.f) * 200.f - 100.f;

    spritesheet_component sprite;
    sprite.flipped = false;
    sprite.FPS = 14;
    sprite.frame.x = 0;
    sprite.frame.y = 0;
    sprite.num_frames = 13;
    sprite.playing = true;
    sprite.resolution.x = 32*3;
    sprite.resolution.y = 32*3;
    sprite.spritesheet = hgeResourcesQueryTexture("tree");
    sprite.time = 0;

    hgeAddComponent(tree, hgeCreateComponent("Transform", &transform, sizeof(transform)));
    hgeAddComponent(tree, hgeCreateComponent("SpriteSheet", &sprite, sizeof(sprite)));
    tree_data.tree = tree;
    hgeAddComponent(chopped, hgeCreateComponent("Tree", &tree_data, sizeof(tree_data)));
}

void cutTree(hge_entity* target, tree_component* data)
{
    hge_transform* chopped_transform = target->components[hgeQuery(target, "Transform")].data;
    createWoodLog(chopped_transform->position.x + TILE, chopped_transform->position.y);
    //createWoodLog(chopped_transform->position.x + TILE*2, chopped_transform->position.y);
    hgeDestroyEntity(data->tree);
    data->tree = NULL;
}

void treeSystem(hge_entity* e, hge_transform* transform, tree_component* data)
{
}
