#include "grass.h"
#include "../../utility/utility.h"

void createBush(float x, float y)
{
    hge_entity* e = hgeCreateEntity();
    hge_transform transform;
    transform.position.x = x;
    transform.position.y = y;
    transform.scale.x = TILE;
    transform.scale.y = TILE;
    transform.scale.z = 0;
    transform.position.z = (fabs(transform.position.y - transform.scale.y/3.f)/1000.f) * 200.f - 100.f;

    spritesheet_component sprite;
    sprite.flipped = false;
    sprite.FPS = 14;
    sprite.frame.x = 1;
    sprite.frame.y = 0;
    sprite.num_frames = 14;
    sprite.playing = true;
    sprite.resolution.x = 32;
    sprite.resolution.y = 32;
    sprite.spritesheet = hgeResourcesQueryTexture("MainTileSheet");
    sprite.time = 0;

    tag_component zaxis;

    hgeAddComponent(e, hgeCreateComponent("Transform", &transform, sizeof(transform)));
    hgeAddComponent(e, hgeCreateComponent("SpriteSheet", &sprite, sizeof(sprite)));
    hgeAddComponent(e, hgeCreateComponent("Zaxis", &zaxis, sizeof(zaxis)));
}

void createFlower(float x, float y)
{
    hge_entity* e = hgeCreateEntity();
    hge_transform transform;
    transform.position.x = x;
    transform.position.y = y;
    transform.scale.x = TILE;
    transform.scale.y = TILE;
    transform.scale.z = 0;
    transform.position.z = (fabs(transform.position.y - transform.scale.y/3.f)/1000.f) * 200.f - 100.f;

    spritesheet_component sprite;
    sprite.flipped = false;
    sprite.FPS = 14;
    sprite.frame.x = 1;
    sprite.frame.y = 1;
    sprite.num_frames = 14;
    sprite.playing = true;
    sprite.resolution.x = 32;
    sprite.resolution.y = 32;
    sprite.spritesheet = hgeResourcesQueryTexture("MainTileSheet");
    sprite.time = 0;

    hgeAddComponent(e, hgeCreateComponent("Transform", &transform, sizeof(transform)));
    hgeAddComponent(e, hgeCreateComponent("SpriteSheet", &sprite, sizeof(sprite)));
}

void loadGrass()
{
    hge_entity* e = hgeCreateEntity();

    hge_transform transform;
    transform.scale.x = 33*TILE;
    transform.scale.y = 39*TILE;
    transform.scale.z = 0;
    transform.position.x = 33/2*TILE;
    transform.position.y = -39/2*TILE;
    transform.position.z = NEAR+1;

    hge_texture sprite = hgeResourcesQueryTexture("grass");

    hgeAddComponent(e, hgeCreateComponent("Transform", &transform, sizeof(transform)));
    hgeAddComponent(e, hgeCreateComponent("Sprite",  &sprite, sizeof(sprite)));
}
