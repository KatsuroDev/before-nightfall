#ifndef BNF_GRASS_H
#define BNF_GRASS_H

#include <HGE/HGE_Core.h>

void createBush(float x, float y);
void createFlower(float x, float y);
void loadGrass();

#endif
