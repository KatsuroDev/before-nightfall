#include "walls.h"
#include "../../utility/utility.h"

void createWalls(float x, float y)
{
    hge_entity* e = hgeCreateEntity();

    hge_transform transform;
    transform.position.x = x;
    transform.position.y = y;
    transform.position.z = NEAR+1;
    transform.scale.x = TILE;
    transform.scale.y = TILE;
    transform.scale.z = 0;

    wall_component wall_data;

    // PHYSICS
        hge_entity* world = hgeQueryEntity(1, "Physic World");
        chipmunk_space* cmS = world->components[hgeQuery(world, "Chipmunk Space")].data;

        wall_data.cmB.body = cpSpaceAddBody(cmS->space, cpBodyNewStatic());
        cpBodySetPosition(wall_data.cmB.body, cpv(x, y));
        addBodyToWorld(wall_data.cmB.body);

        wall_data.cmSp.shape = cpSpaceAddShape(cmS->space, cpBoxShapeNew(wall_data.cmB.body, TILE, TILE, 0));
        cpShapeSetFriction(wall_data.cmSp.shape, 0);
        addShapeToWorld(wall_data.cmSp.shape);

    hgeAddComponent(e, hgeCreateComponent("Wall", &wall_data, sizeof(wall_data)));
    hgeAddComponent(e, hgeCreateComponent("Transform", &transform, sizeof(transform)));
}

void loadWalls()
{
    hge_entity* e = hgeCreateEntity();

    hge_transform transform;
    transform.scale.x = 33*TILE;
    transform.scale.y = 39*TILE;
    transform.scale.z = 0;
    transform.position.x = 33/2*TILE;
    transform.position.y = -39/2*TILE;
    transform.position.z = NEAR+1;

    hge_texture sprite = hgeResourcesQueryTexture("walls");

    hgeAddComponent(e, hgeCreateComponent("Transform", &transform, sizeof(transform)));
    hgeAddComponent(e, hgeCreateComponent("Sprite",  &sprite, sizeof(sprite)));
}
