#ifndef BNF_WALLS_H
#define BNF_WALLS_H

#include <HGE/HGE_Core.h>
#include "../../physics/physics.h"

typedef struct {
    chipmunk_body cmB;
    chipmunk_shape cmSp;
} wall_component;

void createWalls(float x, float y);
// remove the bool;

void loadWalls();


#endif
