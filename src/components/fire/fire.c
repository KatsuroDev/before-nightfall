#include "fire.h"
#include "../../utility/utility.h"
#include "../../game/game.h"
#include "../player/player.h"

void createFire(float x, float y)
{
    hge_entity* e = hgeCreateEntity();
    hge_transform transform;
    transform.position.x = x;
    transform.position.y = y;
    transform.position.z = NEAR+1;
    transform.scale.x = TILE*2;
    transform.scale.y = TILE*2;
    transform.scale.z = 0;

    spritesheet_component sprite;
    sprite.flipped = false;
    sprite.FPS = 13;
    sprite.frame.x = 0;
    sprite.frame.y = 0;
    sprite.num_frames = 7;
    sprite.playing = true;
    sprite.resolution.x = 64;
    sprite.resolution.y = 64;
    sprite.spritesheet = hgeResourcesQueryTexture("fire");
    sprite.time = 0;

    fire_component fire_data;
    hge_transform triggerTransform;
    triggerTransform.position.x = transform.position.x;
    triggerTransform.position.y = transform.position.y;
    triggerTransform.position.z = 0;
    triggerTransform.scale.x = TILE/2;
    triggerTransform.scale.y = TILE/2;
    triggerTransform.scale.z = 0;
    fire_data.triggerArea = triggerTransform;

    hgeAddComponent(e, hgeCreateComponent("Transform", &transform, sizeof(transform)));
    hgeAddComponent(e, hgeCreateComponent("SpriteSheet", &sprite, sizeof(sprite)));
    hgeAddComponent(e, hgeCreateComponent("Fire", &fire_data, sizeof(fire_data)));

}


void FireSystem(hge_entity* e, hge_transform* transform, fire_component* data)
{
    hge_entity* game = hgeQueryEntity(1, "Game");
    game_component* game_data = game->components[hgeQuery(game, "Game")].data;
    if(game_data->state == ALIVE)
    {
        hge_entity* player = hgeQueryEntity(1, "Player");

        hge_transform* player_transform = player->components[hgeQuery(player, "Transform")].data;
        if(AABB(data->triggerArea, *player_transform)) {
            game_data->state = WIN;
            killPlayer();
        }
    }
}
