#ifndef BNF_FIRE_H
#define BNF_FIRE_H

#include <HGE/HGE_Core.h>

typedef struct {
    hge_transform triggerArea;
} fire_component;

void createFire(float x, float y);
void FireSystem(hge_entity* e, hge_transform* transform, fire_component* data);


#endif
