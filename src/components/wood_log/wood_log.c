#include "wood_log.h"
#include "../sign/sign.h"
#include "../../utility/utility.h"

void createWoodLog(float x, float y)
{
    hge_entity* e = hgeCreateEntity();
    hge_transform transform;
    transform.position.x = x;
    transform.position.y = y;
    transform.position.z = 0;
    transform.scale.x = TILE;
    transform.scale.y = TILE;
    transform.scale.z = 0;

    hge_texture sprite = hgeResourcesQueryTexture("wood_log");

    wood_log_component wlog_data;

    // PHYSICS
        hge_entity* world = hgeQueryEntity(1, "Physic World");
        chipmunk_space* cmS = world->components[hgeQuery(world, "Chipmunk Space")].data;

        cpFloat mass = 0.5;
        cpFloat moment = cpMomentForBox(mass, transform.scale.x, transform.scale.y/3);

        wlog_data.cmB.body = cpSpaceAddBody(cmS->space, cpBodyNew(mass, moment));
        cpBodySetPosition(wlog_data.cmB.body, cpv(transform.position.x, transform.position.y));
        addBodyToWorld(wlog_data.cmB.body);

        wlog_data.cmSp.shape = cpSpaceAddShape(cmS->space, cpBoxShapeNew(wlog_data.cmB.body, transform.scale.x/2, transform.scale.y/2, 0));
        cpShapeSetFriction(wlog_data.cmSp.shape, 3);
        addShapeToWorld(wlog_data.cmSp.shape);


    hgeAddComponent(e, hgeCreateComponent("Transform", &transform, sizeof(transform)));
    hgeAddComponent(e, hgeCreateComponent("Sprite", &sprite, sizeof(sprite)));
    hgeAddComponent(e, hgeCreateComponent("Wood Log", &wlog_data, sizeof(wlog_data)));
    hgeAddComponent(e, hgeCreateComponent("Chipmunk Body", &wlog_data.cmB, sizeof(wlog_data.cmB)));
    hgeAddComponent(e, hgeCreateComponent("Chipmunk Shape", &wlog_data.cmSp, sizeof(wlog_data.cmSp)));

    addWoodLogToWorld();

}

void WoodLogSystem(hge_entity* e, hge_transform* transform, wood_log_component* data)
{
    woodLogMovement(transform, data);
    woodLogPhysics(transform, data);
    transform->position.z = (fabs(transform->position.y - transform->scale.y/3.f)/1000.f) * 200.f - 100.f;
}

void woodLogMovement(hge_transform* transform, wood_log_component* data)
{
    cpVect vel = cpBodyGetVelocity(data->cmB.body);
    float acceleration = 2.0f;
    vel.x += (0 - vel.x) * acceleration * hgeDeltaTime();
    vel.y += (0 - vel.y) * acceleration * hgeDeltaTime();
    cpBodySetVelocity(data->cmB.body, vel);
}

void woodLogPhysics(hge_transform* transform, wood_log_component* data)
{
    cpVect pos = cpBodyGetPosition(data->cmB.body);
    cpVect vel = cpBodyGetVelocity(data->cmB.body);

    transform->position.x = pos.x;
    transform->position.y = pos.y;
}

void killWoodLog(hge_entity* target)
{
    remWoodLogToWorld();

    hge_entity* world = hgeQueryEntity(1, "Physic World");
    chipmunk_space* cmS = world->components[hgeQuery(world, "Chipmunk Space")].data;

    wood_log_component* wlog_data = target->components[hgeQuery(target, "Wood Log")].data;

    cpSpaceRemoveBody(cmS->space, wlog_data->cmB.body);
    cpSpaceRemoveShape(cmS->space, wlog_data->cmSp.shape);

    hgeDestroyEntity(target);
}
