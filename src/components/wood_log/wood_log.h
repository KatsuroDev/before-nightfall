#ifndef BNF_WOOD_LOG_H
#define BNF_WOOD_LOG_H

#include <HGE/HGE_Core.h>
#include "../../physics/physics.h"

typedef struct {
    chipmunk_body cmB;
    chipmunk_shape cmSp;
} wood_log_component;

void createWoodLog(float x, float y);
void WoodLogSystem(hge_entity* e, hge_transform* transform, wood_log_component* data);
void woodLogMovement(hge_transform* transform, wood_log_component* data);
void woodLogPhysics(hge_transform* transform, wood_log_component* data);
void killWoodLog(hge_entity* target);

#endif
