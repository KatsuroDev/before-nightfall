#ifndef BNF_WATER_H
#define BNF_WATER_H

#include <HGE/HGE_Core.h>
#include "../../physics/physics.h"

typedef struct {
    chipmunk_body cmB;
    chipmunk_shape cmSp;
} waterphysic_component;

void loadWaterBoundaries();
void createWaterBound(float x, float y);

void createWaterPhysic(float x, float y, int spritenum);
// void deleteAwaterPhysic();

#endif
