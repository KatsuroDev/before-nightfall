#include "water.h"
#include "../../utility/utility.h"

hge_entity* waterphysic[5];
int numWaterPhysic = 0;

void loadWaterBoundaries()
{
    hge_entity* e = hgeCreateEntity();

    hge_transform transform;
    transform.scale.x = 33*TILE;
    transform.scale.y = 39*TILE;
    transform.position.x = 33/2*TILE;
    transform.position.y = -39/2*TILE;
    transform.position.z = NEAR;
    transform.scale.z = 0;

    hge_texture sprite = hgeResourcesQueryTexture("waterBoundaries");

    hgeAddComponent(e, hgeCreateComponent("Transform", &transform, sizeof(transform)));
    hgeAddComponent(e, hgeCreateComponent("Sprite",  &sprite, sizeof(sprite)));
}

void createWaterBound(float x, float y)
{
    hge_transform transform;
    transform.scale.x = TILE;
    transform.scale.y = TILE;
    transform.scale.z = 0;
    transform.position.x = x;
    transform.position.y = y;
    transform.position.z = NEAR;

    hge_entity* world = hgeQueryEntity(1, "Physic World");
    chipmunk_space* cmS = world->components[hgeQuery(world, "Chipmunk Space")].data;

    chipmunk_body cmB;
    cmB.body = cpSpaceAddBody(cmS->space, cpBodyNewStatic());
    cpBodySetPosition(cmB.body, cpv(x, y));
    addBodyToWorld(cmB.body);

    chipmunk_shape cmSp;
    cmSp.shape = cpSpaceAddShape(cmS->space, cpBoxShapeNew(cmB.body, transform.scale.x, transform.scale.y, 0));
    cpShapeSetFriction(cmSp.shape, 0);
    addShapeToWorld(cmSp.shape);
}

void createWaterPhysic(float x, float y, int spritenum)
{
    hge_entity* e = hgeCreateEntity();
    hge_transform transform;
    transform.scale.x = TILE;
    transform.scale.y = TILE;
    transform.scale.z = 0;
    transform.position.x = x;
    transform.position.y = y;
    transform.position.z = NEAR;

    hge_entity* world = hgeQueryEntity(1, "Physic World");
    chipmunk_space* cmS = world->components[hgeQuery(world, "Chipmunk Space")].data;

    waterphysic_component data;

    data.cmB.body = cpSpaceAddBody(cmS->space, cpBodyNewStatic());
    cpBodySetPosition(data.cmB.body, cpv(x, y + transform.scale.y/3));
    addBodyToWorld(data.cmB.body);

    data.cmSp.shape = cpSpaceAddShape(cmS->space, cpBoxShapeNew(data.cmB.body, transform.scale.x/2, transform.scale.y/2, 0));
    cpShapeSetFriction(data.cmSp.shape, 0);
    addShapeToWorld(data.cmSp.shape);

    waterphysic[numWaterPhysic] = e;
    numWaterPhysic++;

    hge_vec2 frame;
    switch (spritenum) {
        case TMX_WATER_TOP:
            frame.x = 6;
            frame.y = 0;
            break;
        case TMX_WATER_BOTTOM:
            frame.x = 6;
            frame.y = 2;
            break;
        case TMX_WATER_FULL:
            frame.x = 6;
            frame.y = 1;
            break;
    }

    spritesheet_component sprite;
    sprite.flipped = false;
    sprite.FPS = 0;
    sprite.frame = frame;
    sprite.num_frames = 0;
    sprite.playing = false;
    sprite.resolution.x = 32;
    sprite.resolution.y = 32;
    sprite.spritesheet = hgeResourcesQueryTexture("WaterTileSheet");
    sprite.time = 0;

    hgeAddComponent(e, hgeCreateComponent("WaterPhysic", &data, sizeof(data)));
    hgeAddComponent(e, hgeCreateComponent("Transform", &transform, sizeof(transform)));
    hgeAddComponent(e, hgeCreateComponent("SpriteSheet", &sprite, sizeof(sprite)));

}
void deleteAwaterPhysic()
{
    int tempNum;
    tempNum = numWaterPhysic;

    hge_entity* world = hgeQueryEntity(1, "Physic World");
    chipmunk_space* cmS = world->components[hgeQuery(world, "Chipmunk Space")].data;

    waterphysic_component* water_data = waterphysic[tempNum-1]->components[hgeQuery(waterphysic[tempNum-1], "WaterPhysic")].data;

    cpSpaceRemoveBody(cmS->space, water_data->cmB.body);
    cpSpaceRemoveShape(cmS->space, water_data->cmSp.shape);

    hgeDestroyEntity(waterphysic[tempNum-1]);
    numWaterPhysic--;
}
