#include "ground.h"
#include "../../utility/utility.h"

void loadGround()
{
    hge_entity* e = hgeCreateEntity();

    hge_transform transform;
    transform.scale.x = 33*TILE;
    transform.scale.y = 39*TILE;
    transform.position.x = 33/2*TILE;
    transform.position.y = -39/2*TILE;
    transform.position.z = NEAR;
    transform.scale.z = 0;

    hge_texture sprite = hgeResourcesQueryTexture("ground");

    tag_component tag;

    hgeAddComponent(e, hgeCreateComponent("Transform", &transform, sizeof(transform)));
    hgeAddComponent(e, hgeCreateComponent("Sprite",  &sprite, sizeof(sprite)));
    hgeAddComponent(e, hgeCreateComponent("Ground",  &tag, sizeof(tag)));
}
