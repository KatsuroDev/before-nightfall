#include "character.h"
#include "../../utility/utility.h"

void CharacterSystem(hge_entity* e, hge_transform* transform, character_component* data)
{
    CharacterMovement(transform, data);
    CharacterPhysics(transform, data);
}


void CharacterMovement(hge_transform* transform, character_component* data)
{
    cpVect vel = cpBodyGetVelocity(data->cmB.body);
    float acceleration = 10.0f;
    switch (data->state) {
        case IDLE:
            vel.x += (0 - vel.x) * acceleration * hgeDeltaTime();
            vel.y += (0 - vel.y) * acceleration * hgeDeltaTime();
            break;
        case MOVING:
            if(data->movement_direction.x < 0)
                transform->scale.x = -TILE;
            if(data->movement_direction.x > 0)
                transform->scale.x = TILE;
            vel.x += (data->movement_direction.x*(data->isRunning ? data->sprint_speed : data->walk_speed) - vel.x) * acceleration * hgeDeltaTime();
            vel.y += (data->movement_direction.y*(data->isRunning ? data->sprint_speed : data->walk_speed) - vel.y) * acceleration * hgeDeltaTime();
            // data->movement_vector.x += (data->movement_direction.x*(data->isRunning ? data->sprint_speed : data->walk_speed) - data->movement_vector.x) * acceleration * hgeDeltaTime();
            // data->movement_vector.y += (data->movement_direction.y*(data->isRunning ? data->sprint_speed : data->walk_speed) - data->movement_vector.y) * acceleration * hgeDeltaTime();
            break;
    }
    //vel.x += data->movement_vector.x * hgeDeltaTime();
    //vel.y += data->movement_vector.y * hgeDeltaTime();
    cpBodySetVelocity(data->cmB.body, vel);
}

void CharacterPhysics(hge_transform* transform, character_component* data)
{
    cpVect pos = cpBodyGetPosition(data->cmB.body);
    cpVect vel = cpBodyGetVelocity(data->cmB.body);

    transform->position.x = pos.x;
    transform->position.y = pos.y;

    //printf("Pos %5.2f, %5.2f\nVel %5.2f, %5.2f\n", pos.x/TILE, pos.y/TILE, vel.x, vel.y);

}

void createCharacter(float x, float y, hge_texture sprite)
{
    hge_entity* e = hgeCreateEntity();

    // TRANSFORM
    hge_transform transform;
    transform.position.x = x;
    transform.position.y = y;
    transform.position.z = 1;
    transform.scale.x = TILE;
    transform.scale.y = TILE;
    hgeAddComponent(e, hgeCreateComponent("Transform", &transform, sizeof(transform)));

    // CHARACTER
    character_component character_data;
    character_data.state = IDLE;
    character_data.movement_direction.x = 0;
    character_data.movement_direction.y = 0;
    character_data.movement_vector.x = 0;
    character_data.movement_vector.y = 0;
    character_data.walk_speed = 25.f;
    character_data.sprint_speed = 40.f;
    character_data.isRunning = false;

        // PHYSICS
        hge_entity* world = hgeQueryEntity(1, "Physic World");
        chipmunk_space* cmS = world->components[hgeQuery(world, "Chipmunk Space")].data;

        cpFloat mass = 1;
        cpFloat moment = cpMomentForBox(mass, transform.scale.x/2, transform.scale.y/2);

        character_data.cmB.body = cpSpaceAddBody(cmS->space, cpBodyNew(mass, moment));
        cpBodySetPosition(character_data.cmB.body, cpv(transform.position.x, transform.position.y));
        addBodyToWorld(character_data.cmB.body);

        character_data.cmSp.shape = cpSpaceAddShape(cmS->space, cpBoxShapeNew(character_data.cmB.body, transform.scale.x/2, transform.scale.y/2, 0));
        cpShapeSetFriction(character_data.cmSp.shape, 0.7);
        addShapeToWorld(character_data.cmSp.shape);

    hgeAddComponent(e, hgeCreateComponent("Character", &character_data, sizeof(character_data)));

    hgeAddComponent(e, hgeCreateComponent("Sprite", &sprite, sizeof(sprite)));
}
