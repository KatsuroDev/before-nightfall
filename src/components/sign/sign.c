#include "sign.h"
#include "../../utility/utility.h"
#include "../plank/plank.h"
#include "../ground/water.h"

void createSign(float x, float y)
{
    hge_entity* e = hgeCreateEntity();

    hge_transform transform;
    transform.position.x = x;
    transform.position.y = y;
    transform.scale.x = TILE;
    transform.scale.y = TILE;
    transform.scale.z = 0;
    transform.position.z = (fabs(transform.position.y - transform.scale.y/4.f)/1000.f) * 200.f - 100.f;

    spritesheet_component sprite;
    sprite.flipped = false;
    sprite.FPS = 0;
    sprite.frame.x = 14;
    sprite.frame.y = 2;
    sprite.num_frames = 0;
    sprite.playing = false;
    sprite.resolution.x = 32;
    sprite.resolution.y = 32;
    sprite.spritesheet = hgeResourcesQueryTexture("MainTileSheet");
    sprite.time = 0;

    sign_component sign_data;
    sign_data.numWoodLog = 0;
    hge_transform triggerTransform;
    triggerTransform.position.x = transform.position.x + TILE;
    triggerTransform.position.y = transform.position.y;
    triggerTransform.position.z = 0;
    triggerTransform.scale.x = TILE/2;
    triggerTransform.scale.y = TILE/2;
    triggerTransform.scale.z = 0;
    sign_data.triggerArea = triggerTransform;

    hgeAddComponent(e, hgeCreateComponent("Transform", &transform, sizeof(transform)));
    hgeAddComponent(e, hgeCreateComponent("SpriteSheet", &sprite, sizeof(sprite)));
    hgeAddComponent(e, hgeCreateComponent("Sign", &sign_data, sizeof(sign_data)));
}

void SignSystem(hge_entity* e, hge_transform* transform, sign_component* data)
{
    if(GetNumWoodLogInWorld() > 0)
    {
        hge_ecs_request request = hgeECSRequest(1, "Wood Log");
        for(int i = 0; i < request.NUM_ENTITIES; i++) {
            hge_transform *wlog_transform = request.entities[i]->components[hgeQuery(request.entities[i], "Transform")].data;
            hge_transform wlog_trigger_transform = *wlog_transform;
            wlog_trigger_transform.scale.x /= 2;
            wlog_trigger_transform.scale.y /= 2;
            if(AABB(data->triggerArea, wlog_trigger_transform)) {
                addWoodLogToSign(request.entities[i], data);
            }
        }
    }
}

void addWoodLogToSign(hge_entity* wlogTarget, sign_component* data)
{
    deleteAwaterPhysic();
    data->numWoodLog++;
    killWoodLog(wlogTarget);
    hge_transform* triggerTransform = &data->triggerArea;
    triggerTransform->position.y += TILE;
    createPlank(triggerTransform->position.x, triggerTransform->position.y);
}
