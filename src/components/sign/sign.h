#ifndef BNF_SIGN_H
#define BNF_SIGN_H

#include <HGE/HGE_Core.h>
#include "../../physics/physics.h"

typedef struct {
    hge_transform triggerArea;
    int numWoodLog;
} sign_component;

void createSign(float x, float y);

void SignSystem(hge_entity* e, hge_transform* transform, sign_component* data);

void addWoodLogToSign(hge_entity* wlogTarget, sign_component* data);

#endif
