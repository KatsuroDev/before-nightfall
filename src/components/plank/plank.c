#include "plank.h"
#include "../../utility/utility.h"

void createPlank(float x, float y)
{
    hge_entity* e = hgeCreateEntity();
    hge_transform transform;
    transform.position.x = x;
    transform.position.y = y;
    transform.position.z = NEAR+1;
    transform.scale.x = TILE;
    transform.scale.y = TILE;
    transform.scale.z = 0;

    hge_texture sprite = hgeResourcesQueryTexture("plank");

    hgeAddComponent(e, hgeCreateComponent("Transform", &transform, sizeof(transform)));
    hgeAddComponent(e, hgeCreateComponent("Sprite", &sprite, sizeof(sprite)));
}
