#include <HGE/HGE_Core.h>
#include <chipmunk/chipmunk.h>
#include "utility/utility.h"
#include "components/player/player.h"
#include "components/character/character.h"
#include "components/fire/fire.h"
#include "components/wood_log/wood_log.h"


int main(int argc, char *argv[])
{
    hge_window window = {"Before Nightfall", 800, 600};
    hgeInit(60, window, HGE_INIT_ALL);

    initGame();

    hgeStart();

    cleanUpGame();
    return 0;
}
