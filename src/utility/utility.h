#ifndef BNF_FUNCTIONS_H
#define BNF_FUNCTIONS_H

#include <HGE/HGE_Core.h>
#include <chipmunk/chipmunk.h>
#include "../TMX/tmx.h"

#define TILE 16

#define NEAR -100.0f
#define FAR 100.0f

#define TMX_BARREL 9
#define TMX_FENCE 24
#define TMX_CHOPPED 8
#define TMX_FLOWER 25
#define TMX_BUSH 10
#define TMX_SIGN 53
#define TMX_WATER_FULL 68
#define TMX_WATER_TOP 60
#define TMX_WATER_BOTTOM 76
#define TMX_WATER_OUT_CORNER_DL 75
#define TMX_WATER_LEFT 67
#define TMX_WATER_RIGHT 69

void loadTextures();

void addAllSystems();

void addCamera();

void cleanUpGame();

void LoadLevel(const char* level_path);

hge_entity* treesWithShape(cpShape *shape);

void addWoodLogToWorld();

void remWoodLogToWorld();

int GetNumWoodLogInWorld();
void removeComponent(hge_entity* entity, int index);

void GameStart();

#endif
