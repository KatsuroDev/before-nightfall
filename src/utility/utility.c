#include "utility.h"
#include "../components/player/player.h"
#include "../components/character/character.h"
#include "../TMX/tmx.h"
#include "../components/ground/ground.h"
#include "../components/ground/water.h"
#include "../components/tree/tree.h"
#include "../components/walls/walls.h"
#include "../components/grass/grass.h"
#include "../components/wood_log/wood_log.h"
#include "../components/fire/fire.h"
#include "../components/sign/sign.h"
#include "../game/game.h"


int numWoodLogInWorld = 0;

void backgroundRendering(hge_entity* e, tag_component* tag)
{
    hgeClearColor(0.098, 0.733, 0.863, 1);
    hgeClear(0x00004000);
}

void loadTextures()
{
    hgeResourcesLoadTexture("./res/Sheets/tilesheet.png", "MainTileSheet");
    hgeResourcesLoadTexture("./res/Sheets/tilesetForWater.png", "WaterTileSheet");
    hgeResourcesLoadTexture("./res/Sheets/chopped.png", "Chopped");
    hgeResourcesLoadTexture("./res/Sheets/ground.png", "ground");
    hgeResourcesLoadTexture("./res/Sheets/walls.png", "walls");
    hgeResourcesLoadTexture("./res/Sheets/grass.png", "grass");
    hgeResourcesLoadTexture("./res/Sheets/tree.png", "tree");
    hgeResourcesLoadTexture("./res/Sheets/fire.png", "fire");
    hgeResourcesLoadTexture("./res/Sheets/zombie characters.png", "characters");
    hgeResourcesLoadTexture("./res/debug.png", "debug");
    hgeResourcesLoadTexture("./res/Sheets/woodlog.png", "wood_log");
    hgeResourcesLoadTexture("./res/Sheets/plank.png", "plank");
    hgeResourcesLoadTexture("./res/Sheets/waterBoundaries.png", "waterBoundaries");
    hgeResourcesLoadTexture("./res/intro/title.png", "Title");
    hgeResourcesLoadTexture("./res/intro/tutorial.png", "Tutorial");
    hgeResourcesLoadTexture("./res/intro/winning.png", "Winning");
    hgeResourcesLoadTexture("./res/intro/difficulty.png", "Difficulty");
    hgeResourcesLoadTexture("./res/intro/start.png", "Start");
    hgeResourcesLoadTexture("./res/intro/win.png", "Won");
    hgeResourcesLoadTexture("./res/intro/dead.png", "Died");
    hgeResourcesLoadTexture("./res/Sheets/timer_sheet.png", "Timer");
}

void addAllSystems()
{
    hgeAddSystem(backgroundRendering, 1, "Ground");
    hgeAddBaseSystems();
    hgeAddSystem(PlayerSystem, 2, "Transform", "Player");
    hgeAddSystem(PlayableSystem, 2, "Playable", "Character");
    hgeAddSystem(CharacterSystem, 2, "Transform", "Character");
    hgeAddSystem(SpaceSystem, 1, "Chipmunk Space");
    hgeAddSystem(WoodLogSystem, 2, "Transform", "Wood Log");
    hgeAddSystem(SignSystem, 2, "Transform", "Sign");
    hgeAddSystem(FireSystem, 2, "Transform", "Fire");
    hgeAddSystem(GameSystem, 1, "Game");
    hgeAddSystem(IntroSystem, 1, "Intro");
}

void addCamera()
{
    hge_entity* camera_entity = hgeCreateEntity();
    hge_camera cam = {true, true, 1.f/5.f, hgeWindowWidth(), hgeWindowHeight(), (float)hgeWindowWidth()/(float)hgeWindowHeight(), NEAR-1.0f, FAR+1.0f };
    hge_vec3 camera_position = {12*TILE, -23*TILE, 0};
    orientation_component camera_orientation = {0.f, -90.0f, 0.f};
    hgeAddComponent(camera_entity, hgeCreateComponent("Camera", &cam, sizeof(cam)));
    hgeAddComponent(camera_entity, hgeCreateComponent("Position", &camera_position, sizeof(camera_position)));
    hgeAddComponent(camera_entity, hgeCreateComponent("Orientation", &camera_orientation, sizeof(camera_orientation)));
    tag_component activecam_tag;
    hgeAddComponent(camera_entity, hgeCreateComponent("ActiveCamera", &activecam_tag, sizeof(activecam_tag)));
}

void initGame()
{
    loadTextures();
    addAllSystems();
    addCamera();

    createGame();

    createPhysicWorld();

    LoadLevel("res/untitled.tmx");
}

void GameStart()
{
    hge_entity* game = hgeQueryEntity(1, "Game");
    game_component* game_data = game->components[hgeQuery(game, "Game")].data;
    game_data->state = ALIVE;

    createPlayer(8*TILE, -23*TILE, true);
}

void ParseTMXObject(tmx_object* object, tmx_map* map) {
  createFire((object->x/map->width)*TILE, (-object->y/map->height)*TILE);
}

void ParseTMXData(tmx_map* map)
{
    tmx_layer* layer = map->ly_head;
    while(layer != NULL) {
        if(strcmp(layer->name, "Ground") == 0) {
            loadGround();
        } else if(strcmp(layer->name, "Walls") == 0) {
            for(int i = 0; i < map->width*map->height; i++) {
                int x = i%map->width;
                int y = i/map->width;
                if(layer->content.gids[i] != 0)
                    createWalls(x*TILE, -y*TILE);
            }
        } else if(strcmp(layer->name, "WallSprite") == 0) {
            loadWalls();
        } else if(strcmp(layer->name, "Grass") == 0) {
            for(int i = 0; i < map->width*map->height; i++) {
                int x = i%map->width;
                int y = i/map->width;
                if(layer->content.gids[i] == TMX_BUSH)
                    createBush(x*TILE, -y*TILE);
                if(layer->content.gids[i] == TMX_FLOWER)
                    createFlower(x*TILE, -y*TILE);
            }
            loadGrass();
        } else if(strcmp(layer->name, "Trees") == 0) {
            for(int i = 0; i < map->width*map->height; i++) {
                int x = i%map->width;
                int y = i/map->width;
                if(layer->content.gids[i] == TMX_CHOPPED)
                    createTree(x*TILE, -y*TILE);
            }
        } else if(strcmp(layer->name, "Sign") == 0) {
            for(int i = 0; i < map->width*map->height; i++) {
                int x = i%map->width;
                int y = i/map->width;
                if(layer->content.gids[i] == TMX_SIGN)
                    createSign(x*TILE, -y*TILE);
            }
        } else if(strcmp(layer->name, "WaterBoundaries") == 0) {
            for(int i = 0; i < map->width*map->height; i++) {
                int x = i%map->width;
                int y = i/map->width;
                if(layer->content.gids[i] == TMX_WATER_TOP || layer->content.gids[i] == TMX_WATER_BOTTOM || layer->content.gids[i] == TMX_WATER_OUT_CORNER_DL || layer->content.gids[i] == TMX_WATER_LEFT || layer->content.gids[i] == TMX_WATER_RIGHT || layer->content.gids[i] == TMX_WATER_FULL)
                    createWaterBound(x*TILE, -y*TILE);
            }
            loadWaterBoundaries();
        }  else if(strcmp(layer->name, "WaterPhysics") == 0) {
            for(int i = 0; i < map->width*map->height; i++) {
                int x = i%map->width;
                int y = i/map->width;
                if(layer->content.gids[i] == TMX_WATER_TOP || layer->content.gids[i] == TMX_WATER_BOTTOM || layer->content.gids[i] == TMX_WATER_FULL)
                    createWaterPhysic(x*TILE, -y*TILE, layer->content.gids[i]);
            }
        } else  {
            tmx_object_group* object_group = layer->content.objgr;
            tmx_object* object = object_group->head;
            while(object) {
                ParseTMXObject(object, map);
                object = object->next;
            }
        }
        layer = layer->next;
    }

}

void LoadLevel(const char* level_path)
{
    printf("Loading Level '%s'\n", level_path);
    tmx_map *map = tmx_load(level_path);
    if(!map) {tmx_perror("Cannot load map"); return;}
    ParseTMXData(map);
    tmx_map_free(map);
}

void cleanUpGame()
{
    freeAllPhysics();
}

void addWoodLogToWorld()
{
    numWoodLogInWorld++;
}

void remWoodLogToWorld()
{
    numWoodLogInWorld--;
}

int GetNumWoodLogInWorld()
{
    return numWoodLogInWorld;
}

void removeComponent(hge_entity* entity, int index) {
  free(entity->components[index].data);
  for(int i = index; i < entity->numComponents; i++) {
    entity->components[i] = entity->components[i+1];
  }
  entity->numComponents--;
}
