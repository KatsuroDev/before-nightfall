#ifndef HGE_FILE_UTILITY_H
#define HGE_FILE_UTILITY_H

#include "HGE_Texture.h"

char* hgeLoadFileAsString(const char* path);
hge_texture hgeLoadTexture(const char* path);

#endif
