#ifndef GAME_MATH_H
#define GAME_MATH_H

#include "HGE_Math3D.h"
#include <stdbool.h>

bool AABB(hge_transform A, hge_transform B);

#endif
