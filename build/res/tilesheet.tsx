<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.2" name="tilesheet" tilewidth="32" tileheight="32" tilecount="45" columns="15">
 <image source="Sheets/tilesheet.png" width="480" height="96"/>
 <tile id="0">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16.0526" y="15.5263"/>
   <object id="2" x="5.13158" y="0.526316"/>
   <object id="3" x="19.4737" y="8.94737"/>
   <object id="4" x="19.4737" y="8.94737"/>
   <object id="5" x="16.5789" y="11.0526"/>
   <object id="6" x="16.5789" y="11.0526"/>
   <object id="7" x="16.5789" y="11.0526"/>
  </objectgroup>
 </tile>
</tileset>
