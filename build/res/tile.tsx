<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.2" name="tile" tilewidth="96" tileheight="96" tilecount="8" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="32" height="32" source="Sheets/tile.png"/>
 </tile>
 <tile id="1">
  <image width="64" height="64" source="GuttyKreumNatureTilesvol1/riverbottom64x64transparentanimatednotbright.gif"/>
 </tile>
 <tile id="2">
  <image width="64" height="64" source="GuttyKreumNatureTilesvol1/riverbottomleft64x64.gif"/>
 </tile>
 <tile id="3">
  <image width="64" height="64" source="GuttyKreumNatureTilesvol1/rivertop64x64transparentanimated.gif"/>
 </tile>
 <tile id="4">
  <image width="64" height="64" source="GuttyKreumNatureTilesvol1/rivertopleft64x64transparent.gif"/>
 </tile>
 <tile id="5">
  <image width="96" height="96" source="GuttyKreumNatureTilesvol1/tree96x96transparent.png"/>
 </tile>
 <tile id="6">
  <image width="96" height="96" source="GuttyKreumNatureTilesvol1/tree96x96transparentanimated.gif"/>
 </tile>
 <tile id="7">
  <image width="96" height="96" source="GuttyKreumNatureTilesvol1/chopped96x96.gif"/>
  <objectgroup draworder="index" id="2">
   <object id="5" x="27" y="62" width="37" height="27"/>
  </objectgroup>
 </tile>
</tileset>
