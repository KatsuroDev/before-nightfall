![License](https://img.shields.io/badge/license-MIT-informational)
# Before Nightfall

## Game Jam theme
**Observe the fire from the opposite shore.**
## Gameplay 
Cut trees to make a bridge to the fire on the opposite shore before the night fall, else you will sleep and surely die in darkness.

### Build For Windows
    make BUILD_OS=Win32
### Build For Linux
    make


## How to Play

 - Use SpaceBar to get through the intro.
 - Move around with *WASD* and sprint with **left shift**.
 - Cut trees with the SpaceBar whenever you're near one.
 - Push the logs with physics.
 - Build the bridge on the right side of the sign to get to the fire, by pushing logs into the water.

## Engine

Using the [Hyper Game Engine](https://gitlab.com/Elatronion/hyper-game-engine) by Ezra Hradecky.

## Assets Credits

[Field of Green](https://guttykreum.itch.io/field-of-green) by guttykreum.

[Zombie Characters](https://jeresikstus.itch.io/zombie-characters-32x32) by jeresikstus.

[Serene Village](https://limezu.itch.io/serenevillagerevamped) by limezu.

[Trees](https://jestan.itch.io/trees) by jestan.

# License
Copyright 2020 © Alexis Millette

Before Nightfall is available under the [MIT license](https://mit-license.org/).

```
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```